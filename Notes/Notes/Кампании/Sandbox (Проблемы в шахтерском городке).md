## Информация

---
- ~~Рэкс (Дядя) [Человек, М, 60] - (Разнорабочий) уволенный ветеринар, балабол.~~
- Лэйси [Тифлинг, Ж, 25] (Юля) - (Авантюрист) **
- Требушет [Гном, М, 54] (Сергей) - (Раб) низкий рост, надсмоторщик ** 
- ~~Тэйя (Повелительница флейт) [Человек, Ж, 20] - (Авантюрист)~~
- Раджа [Орк, М, 20] (Илья) - (Раб) Рудокоп
- ~~Ксирис [Хафлинг, Ж, 36] - (Авантюрист)~~
- Варис  [Эльф, М, 154] (Даша) - (Врач), Бывший семьянин **
- Ложная Полли [Человек, Ж, 14] (Женя) - (Раб) Разгрузка **
- Ларра [Человек, Ж, 30] (София) - (Авантюрист)
---
Доступные роли для игроков (стоимость):
- Военный (10) - оружие, владение оружием или доспехом
- Авантюрист (8) - оружие, навык, владение инструментом
- Профессионал (7) - навык, владение инструментом
- Доктор (6) - навык, владение инструментом
- Клерк (5) - навык
- Разнорабочий (4) - владение инструментом
- Обслуга (2) - 50 золота
- Раб (0) - нихуя

Шоп:
- Навык (6)
- Очко характеристик (5)
- Владение оружием или доспехом (4)
- Владение инструментом (2)
- 10 золота (1)
- 100 опыта (1)

---

| Мораль | Еда |   Численность   | Раскопки | Исследование | Ресурсы |
|:------:|:---:|:---------------:|:--------:|:------------:|:-------:|
|   -2   | 14  | 329 (162 закл.) |   43%    |      8%      |   18    |
|        |     |                 |    --    |              |         |

* Преимущество на след. проверку на поиск растений в шахте

|                       Разовое мероприятие                        | Прогресс |             Результат             |
|:----------------------------------------------------------------:|:--------:|:---------------------------------:|
| ~~Лечение раненых после восстания (Бросок медицины от доктора)~~ | ~~2/3~~  |              Провал               |
|   ~~Лечение больных после осложнений от ранений (55 человек)~~   | ~~8/7~~  |               Успех               |
|          Расследование того, что стало с грибной фермой          |   0/1    |                Нет                |
|                  Изучение синдрома правой руки                   |   2/3    |                 ?                 |
|           ~~Информационная кампания "Списите ебаку"~~            | ~~2/2~~  |       Ебака выростит голову       |
|             ~~Изготовление яда для Фавиан (Варис)~~              | ~~3/3~~  | Нейротропный контактный яд (4 шт) | 

### Дестини поинты
Игроков: 2
Мастера: 11

### Время
Дней: 6
Недель: 3
Месяцев: 1

### Эффекты от морали:
- +5 - дополнительный 1% раскопок и исследований
- +4 - снижение потребления еды на 1
- +3 - бонус +3 к убеждению представлению и обману, -3 к запугиванию
- +2 - бонус +2 к убеждению представлению и обману, -2 к запугиванию
- +1 - бонус +1 к убеждению представлению и обману, -1 к запугиванию
-  0 - ничего

### Ролы ебаки:
- Где ебака - `dice: 1d4`

## Места

### Шахты (1) (`dice: 1d2`)
Эдмунд [Человек, М, 39] - Разнорабочий (Бригадир)

#### Проходы (A)
Уолтер [Человек, М, 47] - Разнорабочий (Шахтер)
Елена [Полу-эльф, Ж, 29] - Авантюрист (Разведчик)

#### Станция (B)
Зерекс [Человек, М, 38] - Разнорабочий (Грузчик)

### Общий зал (2) (`dice: 1d3`)
Роза [Человек, Ж, 80] - Военный (Законник)
Мили [Дварф, Ж, 365] - Военный (Законник)
Джозеф [Человек, М, 59] -  (Законник)

#### Лазарет (A)
~~Джанши [Человек, Ж, 23] - Доктор (Фельдшер)~~

#### Склад (B)
Генадий Горин [Человек, М, 90] - Завхоз (Клерк)

#### Администрация (C)
Фавиан [Полу-эльф, Ж, 43] - Профессионал (Лидер экспедиции)
Бэт [Человек, Ж, 23] - Профессионал (Глава безопасности)
Джон МакКиннон [Человек, М, 45] - Профессионал (Инвестор)

### Столовая (3) (`dice: 1d3`)
Фира [Хафлинг, Ж, 20] - Обслуга (Начальник смены столовой)
Гилберт [Дварф, М, 43] - Авантюрист (Проводник)

#### Кухня (A)
Хали [Дварф, М, 128] - Обслуга (Повар)

#### Продовольственный склад (B)

#### Общая комната (C)

### Стоянка (4)  (`dice: 1d3`)

#### Зал рабов (A)

#### Купол коммонеров (B)

#### Купол знати (C)
